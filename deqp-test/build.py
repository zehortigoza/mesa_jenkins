#!/usr/bin/env python3

import sys
import os
sys.path.append(os.path.join(os.path.dirname(os.path.abspath(sys.argv[0])),
                             "..", "repos", "mesa_ci", "build_support"))
from testers import DeqpTester, DeqpSuiteLister, DeqpTrie, ConfigFilter
from build_support import build
from options import Options
from project_map import ProjectMap
from utils.utils import get_conf_file, Flakes, write_disabled_tests


class SlowTimeout:
    def __init__(self):
        self.hardware = Options().hardware

    def GetDuration(self):
        return 500

        
class DeqpBuilder(object):
    def __init__(self):
        self.pm = ProjectMap()
        self.o = Options()
        self.env = {}
        if "crocus" in self.o.hardware:
            self.env = { "MESA_LOADER_DRIVER_OVERRIDE" : "crocus" }
        elif "zink" in self.o.hardware:
            if self.o.arch == "m64":
                icd_name = "intel_icd.x86_64.json"
                hasvk_icd_name = "intel_hasvk_icd.x86_64.json"
            elif self.o.arch == "m32":
                icd_name = "intel_icd.i686.json"
                hasvk_icd_name = "intel_hasvk_icd.i686.json"
            icd_path = f"{self.pm.build_root()}/share/vulkan/icd.d/{icd_name}"
            hasvk_icd_path = f"{self.pm.build_root()}/share/vulkan/icd.d/{hasvk_icd_name}"
            if os.path.exists(hasvk_icd_path):
                icd_path = f"{icd_path}:{hasvk_icd_path}"
            self.env = {
                "MESA_LOADER_DRIVER_OVERRIDE" : "zink",
                "VK_ICD_FILENAMES": icd_path,
            }
        self.version = None
    def build(self):
        pass
    def clean(self):
        pass

    def supports_gles_3(self):
        if ("g33" in self.o.hardware or
            "g45" in self.o.hardware or
            "g965" in self.o.hardware or
            "ilk" in self.o.hardware):
            return False
        return True

    def supports_gles_31(self):
        if not self.supports_gles_3():
            return False
        if "snb" in self.o.hardware:
            return False
        return True

    def test(self):
        if "hsw" in self.o.hardware or "byt" in self.o.hardware or "ivb" in self.o.hardware:
            self.env["MESA_GLES_VERSION_OVERRIDE"] = "3.1"
        tester = DeqpTester()
        all_results = DeqpTrie()

        modules = ["gles2", "egl"]

        if self.supports_gles_3():
            modules += ["gles3"]
        if self.supports_gles_31():
            modules += ["gles31"]

        for module in modules:
            flakes = Flakes(self.pm.current_project(), self.o)
            binary = self.pm.build_root() + "/opt/deqp/modules/" + module + "/deqp-" + module
            lister = DeqpSuiteLister(binary, flakes)
            results = tester.test(binary,
                             lister,
                             [],
                             self.env)
            tester.test_flaky(binary, lister)
            all_results.merge(results)
            write_disabled_tests(self.pm, self.o, lister.tests(self.env), suffix=f"_{module}")

        lister.flakes.generate_xml()
        config = get_conf_file(self.o.hardware, self.o.arch, project=self.pm.current_project())
        tester.generate_results(all_results, ConfigFilter(config, self.o))

if __name__ == '__main__':
    build(DeqpBuilder(), time_limit=SlowTimeout())
