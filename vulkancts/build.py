#!/usr/bin/env python3

import os
import sys

sys.path.append(os.path.join(os.path.dirname(os.path.abspath(sys.argv[0])),
                             "..", "repos", "mesa_ci", "build_support"))
from build_support import build
from export import Export
from options import Options
from project_map import ProjectMap
from repo_set import checkout_externals, apply_patches
from testers import deqp_external_revisions
from utils.command import run_batch_command
from utils.utils import git_clean
from utils.utils import get_package_config_path
import repo_set

def get_external_revisions(revisions_dict=None):
    return deqp_external_revisions(project="vulkancts",
                                   revisions_dict=revisions_dict)


class VulkanCtsBuilder(object):
    def __init__(self):
        self._pm = ProjectMap()
        self._options = Options()
        self._src_dir = self._pm.project_source_dir()
        self._build_dir = self._src_dir + "/build_" + self._options.arch
        self._build_root = self._pm.build_root()

    def build(self):
        apply_patches(self._pm.project_build_dir(), self._src_dir)

        revisions = get_external_revisions()
        external_dir = (self._pm.project_source_dir('vulkancts')
                        + "/external/{}/src")
        # hack for now
        repo_set.repo_set.repo_to_external["vk-video-parser"] = "nvidia-video-samples"

        checkout_externals(project='vulkancts', revisions=revisions,
                           external_dir_format=external_dir)

        btype = "Release"
        # Vulkan cts is twice as slow for RelDeb builds, which impacts
        # the CI throughput.  For this reason, we unconditionally
        # optimize the build.
        # if self._options.config == "debug":
        #    btype = "RelDeb"
        flags = "-m64"
        if self._options.arch == "m32":
            # sse flags are needed to fix FP math rounding issues on some vk
            # cts tests
            flags = "-m32 -msse2 -mfpmath=sse"
        cmd = ["cmake", "-GNinja", "-DCMAKE_BUILD_TYPE=" + btype,
               "-DCMAKE_C_COMPILER_LAUNCHER=ccache",
               "-DCMAKE_CXX_COMPILER_LAUNCHER=ccache",
               "-DCMAKE_C_FLAGS=" + flags, "-DCMAKE_CXX_FLAGS=" + flags,
               "-DCMAKE_C_COMPILER=gcc", "-DCMAKE_CXX_COMPILER=g++",
               "-DCMAKE_INSTALL_PREFIX:PATH=" + self._build_root, ".."]
        env = {"CC":"ccache gcc",
               "CXX":"ccache g++",
               "CFLAGS":flags,
               "CXXFLAGS":flags,
               "PKG_CONFIG_PATH":get_package_config_path()}

        if not os.path.exists(self._build_dir):
            os.makedirs(self._build_dir)
        os.chdir(self._build_dir)
        run_batch_command(cmd, env=env)
        run_batch_command(["ninja"])
        bin_dir = self._build_root + "/opt/deqp/"
        if not os.path.exists(bin_dir):
            os.makedirs(bin_dir)

        run_batch_command(["rsync", "-rlptD",
                           self._build_dir + "/external/vulkancts/modules",
                           bin_dir])

        Export().export()

    def clean(self):
        git_clean(self._src_dir)

    def test(self):
        pass


if __name__ == "__main__":
    build(VulkanCtsBuilder())
