#!/usr/bin/env python3
# Copyright (C) Intel Corp.  2014.  All Rights Reserved.

# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:

# The above copyright notice and this permission notice (including the
# next paragraph) shall be included in all copies or substantial
# portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE COPYRIGHT OWNER(S) AND/OR ITS SUPPLIERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#  **********************************************************************/
#  * Authors:
#  *   Mark Janes <mark.a.janes@intel.com>
#  **********************************************************************/


import sys
import os
import time
import stat
import tempfile
sys.path.append(os.path.join(os.path.dirname(os.path.abspath(sys.argv[0])),
                             "..", "repos", "mesa_ci", "build_support"))
from utils.command import run_batch_command
from export import convert_rsync_path

# from http://stackoverflow.com/questions/6879364/print-file-age-in-seconds-using-python
def file_age_in_seconds(pathname):
    return time.time() - os.stat(pathname)[stat.ST_MTIME]

def file_age_in_days(pathname):
    return file_age_in_seconds(pathname) / (60*60*24)

def rsync_rmtree(pathname):
    if "perforate" in pathname:
        print(f"WARN: skipping cleanup of perforate squashfs image: {pathname}")
        return
    rsync_path = convert_rsync_path(pathname)
    with tempfile.TemporaryDirectory() as temp_dir:
        # per https://www.suse.com/support/kb/doc/?id=000019018
        run_batch_command(['rsync', '-a', '--delete', temp_dir + '/', rsync_path + '/'])

result_path = "/mnt/jenkins/results/"

with tempfile.TemporaryDirectory() as temp_result_path:
    rsync_result_path = convert_rsync_path(result_path)
    # only recurse 2 levels on this rsync command and preserve timestamps, copy symlinks as symlinks
    # modified from https://unix.stackexchange.com/questions/178362/rsync-recursively-with-a-certain-depth-of-subfolders
    run_batch_command(['rsync', '-tr', '--exclude=/*/*/*/', rsync_result_path, temp_result_path])

    for a_dir in os.listdir(temp_result_path):
        sub_dir = os.path.join(result_path, a_dir)
        temp_sub_dir = os.path.join(temp_result_path, a_dir)

        for a_build_dir in os.listdir(temp_sub_dir):
            build_dir = os.path.join(sub_dir, a_build_dir)
            temp_build_dir = os.path.join(temp_sub_dir, a_build_dir)

            if os.path.islink(temp_build_dir):
                continue
            if file_age_in_days(temp_build_dir) > 30:
                rsync_rmtree(build_dir)
