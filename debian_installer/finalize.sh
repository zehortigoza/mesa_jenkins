# Copyright (C) Intel Corp.  2014-2020.  All Rights Reserved.

# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:

# The above copyright notice and this permission notice (including the
# next paragraph) shall be included in all copies or substantial
# portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE COPYRIGHT OWNER(S) AND/OR ITS SUPPLIERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#  **********************************************************************/
#  * Authors:
#  *   Dylan Baker <dylanx.c.baker@intel.com>
#  **********************************************************************/

# Disable S3/suspend/hibernate
systemctl mask sleep.target suspend.target hibernate.target hybrid-sleep.target
sed -i 's|#HandleLidSwitch=suspend|HandleLidSwitch=ignore|' /etc/systemd/logind.conf

#configure apt
mv /etc/apt/sources.list /etc/apt/sources.list.bak
cat > /etc/apt/sources.list << EOF
deb http://linux-ftp.jf.intel.com/pub/mirrors/debian/ testing main contrib non-free non-free-firmware
EOF
DEBIAN_FRONTEND_BAK=$DEBIAN_FRONTEND
DEBIAN_PRIORITY_BAK=$DEBIAN_PRIORITY
export DEBIAN_FRONTEND=noninteractive
export DEBIAN_PRIORITY=critical
apt-get update -y

#install microcode to prevent poor performance
apt-get install -y intel-microcode

####configure docker for CI jobs
apt-get install -y docker-compose
usermod -a -G docker jenkins
systemctl enable docker
cat > /etc/docker/daemon.json << EOF
{
  "storage-driver": "overlay2",
  "dns": ["192.168.1.1", "10.248.2.1"],
  "insecure-registries" : ["otc-mesa-ci.jf.intel.com:5000", "otc-mesa-ci.local:5000"]
}
EOF

#configure tsocks
apt-get install -y tsocks
cat > /etc/tsocks.conf << EOF
local = 192.168.0.0/255.255.0.0
local = 134.134.0.0/255.255.0.0
local = 10.0.0.0/255.0.0.0
server = 10.1.192.48
server_type = 4
server_port = 1080
EOF

# Enable RVP Ethernet port with random MAC Address
cat > /etc/systemd/network/01-mac.link << EOF
[Match]
PermanentMACAddress=88:88:88:88:87:88

[Link]
MACAddressPolicy=random
EOF


#configure ip tables for docker
apt-get install -y iptables
update-alternatives --set iptables /usr/sbin/iptables-legacy
echo "net.ipv4.ip_forward = 1" >> /etc/sysctl.conf

# allow other users to use dmesg
cat > /etc/sysctl.d/60-unprivileged-dmesg.conf << EOF
kernel.dmesg_restrict=0
EOF

#install java and git to connect with and run jenkins jobs
apt-get install -y default-jre python3-git

#install sudo for next steps
apt-get install -y sudo

#update jenkins sudoers file
usermod -aG sudo jenkins
cat > /etc/sudoers.d/10-jenkins-reboot << EOF
jenkins   ALL=(root: root) NOPASSWD: ALL
EOF

#allow jenkins to run dpkg (for installing custom kernels, etc)
cat > /etc/sudoers.d/jenkins_passwordless_dpkg << EOF
jenkins ALL=NOPASSWD: /usr/bin/dpkg
EOF


#install ssh key for jenkins user
mkdir -p /home/jenkins/.ssh
cat > /home/jenkins/.ssh/authorized_keys << EOF
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCiFl/afTIOYeDXoom0tg0T4EmpQSOCdX1Jn3/Jh3nFIByfwTzlhzsBI0YFo3eAXxSwfv1IOgH3RA92K0dfEZooJwlxYMLGQ2v+XbwwzlsEOUscUEGtX+D5vMiIebHievRL8nzAUtLj8L2gQf5WtDwN+wdGh3XriSFVzLiZlWBiBw+97TMh18CqemqbfmZ7BZnRQAXx+hj8QtkQrPaiu7RKCbv2v1Nf3lDhR7vRSEQh3xT0RCoaLYyhXSu1brosUPlo/FyuM2ZWbpthPBpfHLkTLI88L/q2K/zfdpMfGI8O0kLt4aPUkvU+MMmfSB19uNEGsMJ3fRd6PzE3Nkzle9tn jenkins@otc-mesa-ci
ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIJzd9morAT1YtKpHVza8xBjqQJdMNBTLNG4h3UEEK4zI jenkins@otc-mesa-ci
EOF
chown jenkins:jenkins -R /home/jenkins/.ssh

#disable ssh pssword athentication
sed -E -i 's|^#?(PasswordAuthentication)\s.*|\1 no|' /etc/ssh/sshd_config
if ! grep '^PasswordAuthentication\s' /etc/ssh/sshd_config; then echo 'PasswordAuthentication no' | tee -a /etc/ssh/sshd_config; fi


#install ansible and configure ansible user account
apt-get install -y ansible
useradd -m -p '$1$zB4MA3n2$.jP0rGnBao1XG3m.7BV6X1' -s /bin/bash ansible
usermod -aG sudo ansible
mkdir -p /home/ansible/.ssh
chown ansible.ansible /home/ansible/.ssh
cat > /home/ansible/.ssh/authorized_keys << EOF
ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIED4Z9I9XUoHy2KgH2ShwDZNuHDQsWxlXrV+EuMhfL3O ansible@otc-mesa-ci
ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIJzd9morAT1YtKpHVza8xBjqQJdMNBTLNG4h3UEEK4zI jenkins@otc-mesa-ci
EOF
chown ansible.ansible /home/ansible/.ssh/authorized_keys
cat > /etc/sudoers.d/ansible << EOF
ansible ALL=(ALL) NOPASSWD: ALL
EOF

# upgrade all packages to their testing versions (since we just updated
# sources to testing above)
# the stable version of docker doesn't work with our automation currently
apt-get -o Dpkg::Options::="--force-confdef --force-confnew" dist-upgrade -q -y --force-yes

# Enable some services, install guc firmware, rsync
apt-get install -y avahi-daemon fwupd thermald lm-sensors firmware-misc-nonfree rsync firmware-linux firmware-linux-nonfree

#update grub to boot from kernel menu option in grub
rm /etc/default/grub
cat > /etc/default/grub << EOF
GRUB_DEFAULT=0
GRUB_TIMEOUT=5
GRUB_DISTRIBUTOR=`lsb_release -i -s 2> /dev/null || echo Debian`
GRUB_CMDLINE_LINUX_DEFAULT="quiet"
# extra params from: https://make-linux-fast-again.com
GRUB_CMDLINE_LINUX="pcie_aspm=off i915.force_probe=* noibrs noibpb nopti nospectre_v2 nospectre_v1 l1tf=off nospec_store_bypass_disable no_stf_barrier mds=off tsx=on tsx_async_abort=off mitigations=off"
EOF

update-grub

# create an 8GB swap file.  The default 1GB swap is not sufficient for
# systems with 8G RAM to run vulkancts
/sbin/swapon -s
fallocate -l 8g /swapfile
chmod 600 /swapfile
/sbin/mkswap /swapfile
/sbin/swapon /swapfile
/sbin/swapon -s
cat >> /etc/fstab <<EOF
/swapfile         none            swap    sw              0       0
EOF

export DEBIAN_FRONTEND=$DEBIAN_FRONTEND_BAK
export DEBIAN_PRIORITY=$DEBIAN_PRIORITY_BAK
