#!/usr/bin/env python3

import os
import sys
import os.path as path
sys.path.append(path.join(path.dirname(path.abspath(sys.argv[0])), "..",
                          "repos", "mesa_ci", "build_support"))
from build_support import build
from builders import MesonBuilder
from options import Options
from project_map import ProjectMap
from project_invoke import ProjectInvoke
from repo_set import RevisionSpecification, RepoSet

def main():
    global_opts = Options()
    sdir = ProjectMap().project_source_dir()

    options = [
        '-Dtools=intel,drm-shim',
        '-Dglvnd=true',
        '-Dgallium-drivers=swrast,iris,crocus,zink',
        '-Dvulkan-drivers=intel,intel_hasvk',
        '-Dplatforms=x11',
        '-Dbuild-tests=true',
        '-Dinstall-intel-gpu-tests=true'
    ]

    if os.path.exists(f"{sdir}/src/intel/dev/xe/intel_device_info.h"):
        options.append('-Dintel-xe-kmd=enabled')
    if global_opts.arch == 'm64':
        options.extend(['-Dintel-clc=enabled', '-Dllvm=enabled'])
    else:
        options.append('-Dllvm=disabled')

    cpp_args = None
    if global_opts.config == 'debug':
        # default buildtype is debugoptimized.

        # only applies to 64 bit binaries, overridden by cross file.
        # DEBUG was removed from debugoptimized because it is slow.
        cpp_args = "-DDEBUG"
    else:
        # WARN: 32 bit release builds will have -DDEBUG due to cross file (and
        # be slow)
        options.extend(['-Dbuildtype=release', '-Db_ndebug=true'])

    builder = MesonBuilder(extra_definitions=options, install=True,
                           cpp_args=cpp_args)
    build(builder)

if __name__ == '__main__':
    main()
